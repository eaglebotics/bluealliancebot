import tbapy
import os
import datetime

TBA = tbapy.TBA(os.environ['TBA_TOKEN'])
BASE_URL = "https://www.thebluealliance.com"

def getTeam(teamnum):
    # Creating the URL
    blueallianceURL = BASE_URL + "/team/" + str(teamnum)
    # Getting actual info with libraries
    team = TBA.team(teamnum)
    return TBA.team_years(teamnum), team.nickname, team.city + ", " + team.state_prov, blueallianceURL

def getAwards(teamnum):
    awards = []

    for award in TBA.team_awards(teamnum):
        awards.append(award.name + " (" + str(award.year) + ")")

    return awards

def teamEvents(teamnum):
    now = datetime.datetime.now()
    events = TBA.team_events(teamnum, year=now.year, keys=True)
    urls = []

    for event in events:
        urls.append(BASE_URL + "/event/" + event)

    return urls
