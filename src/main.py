HELP = """
        Usage: !tba [command]
        Where [command] is one of the following:\n
        - team [team #]: gets info of team at [number]
        - awards [team #]: gets awards team has won (with years)
        - events [team #]: gets info on team's events this year
        \n**Run all commands without any square brackets.**
        """

#import discord
from discord.ext import commands
import bluealliance
import os

description = 'Fetches info from TheBlueAlliance.'
bot = commands.Bot(command_prefix='!tba ', description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command(description='Display help message.')
async def tba():
    await bot.say(HELP)


@bot.command(description='Gets information regarding a team.')
async def team(id : int):
    years, nickname, location, blueallianceURL = bluealliance.getTeam(id)
    await bot.say("\n**Nickname**: " + nickname
                  + "\n**Location**: " + location
                  + "\n**TBA Page**: " + blueallianceURL
                  + "\n**Years Active**: " + str(years))

@bot.command(description='Gets team awards')
async def awards(teamnumber : int):
    award_list = bluealliance.getAwards(teamnumber)
    awards = str(teamnumber) + " has recieved: \n"

    for award in award_list:
        awards += (award + "\n")

    await bot.say(awards)

@bot.command(description='gets info on next/current event')
async def events(teamnumber : int):
    event_list = bluealliance.teamEvents(teamnumber)
    toprint = str(teamnumber) + "'s events this year:"

    for event in event_list:
        toprint += ("\n" + event)

    await bot.say(toprint)

bot.run(os.environ['DISCORD_TOKEN'])
