# BlueAllianceBot

Info from [TheBlueAlliance](https://www.thebluealliance.com/) served hot and ready right to your Discord server.

**Currently under development; more features to come!**

Click [this link](https://discordapp.com/api/oauth2/authorize?client_id=421338722391425024&permissions=2048&scope=bot) to add the bot to your server.

*We only ask for the permission to send messages.*

Get a list of commands with `!tba tba`, or see below.

## Commands

Usage: !tba [command] (where [command] is replaced by one of the following):

- team [team #]: gets info of team at [number]
- awards [team #]: gets awards team has won (with years)
- events [team #]: gets links to team's events this year on TBA

## Run It Yourself

This repo contains a `Procfile`, which can be used with [Heroku](http://heroku.com/) to run an instance of this bot.